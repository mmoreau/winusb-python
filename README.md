# WinUSB

Un script permettant 

* Autoriser / Interdit les supports de stockages USB d'être inséré dans les ports USB de la machine physique.
* Autoriser / Interdit la copie des fichiers, dossiers vers des supports de stockage USB.

## Dependencies

* Python 3 (https://www.python.org/downloads/release/python-372/)

## Script
### **Windows**
```python
    from Class.Usb import USB 
    
    # USB.storage(True)
    # USB.writeProtect(True)
```

## Todo
- [ ] Windows (USB Tracking)
- [ ] Linux
- [ ] MacOS

## Contact

Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered 

* **Twitter :** https://twitter.com/mxmmoreau
* **LinkedIn :** https://www.linkedin.com/in/mxmoreau/