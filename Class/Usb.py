try:
    import ctypes
    import platform as plf 
    import winreg as wr 
    from Registry import Registry as rg 
except ModuleNotFoundError as error:
    print(error)


class USB:

    @classmethod
    def storage(cls, state):

        """Autorise / Interdit les supports de stockages USB d'être inséré dans les ports USB de la machine physique."""

        """
            3 # Autorise les supports de stockages USB d'être inséré dans les ports USB.
            4 # Interdit les supports de stockages USB d'être inséré dans les ports USB
        """

        if isinstance(state, bool):
            if plf.system() == "Windows":
                if ctypes.windll.shell32.IsUserAnAdmin():

                    path = r"SYSTEM\CurrentControlSet\Services\USBSTOR"
                    key = rg.ROOT["HKLM"]
                    value = "Start"

                    if rg.pathExist(key, path):
                        with wr.OpenKey(key, path, 0, wr.KEY_ALL_ACCESS) as hkey:
                            wr.SetValueEx(hkey, value, 0, rg.TYPE["DWORD"], [4, 3][state])

    
    @classmethod
    def writeProtect(cls, state):

        """Autorise / Interdit la copie des fichiers, dossiers vers des supports de stockage USB."""

        """
            True # Interdit la copie des fichiers, dossiers vers des supports de stockages USB.
            False # Autorise la copie des fichiers, dossiers vers des supports de stockages USB.
        """

        if isinstance(state, bool):
            if plt.system() == "Windows":
                if ctypes.windll.shell32.IsUserAnAdmin():

                    path = r"SYSTEM\CurrentControlSet\Control\StorageDevicesPolicies"
                    key = rg.ROOT["HKLM"]
                    value = "WriteProtect"

                    if not rg.pathExist(key, path):
                        wr.CreateKey(key, path)
                    
                    with wr.OpenKey(key, path, 0, wr.KEY_ALL_ACCESS) as hkey:
                        wr.SetValueEx(hkey, value, 0, rg.TYPE["DWORD"], [0, 1][state])