try:
	import winreg as wr
except ModuleNotFoundError as error:
	print(error)
	

class Registry:


	# https://en.wikipedia.org/wiki/Windows_Registry#Root_keys
	ROOT = {
		"HKCR": wr.HKEY_CLASSES_ROOT,
		"HKCU": wr.HKEY_CURRENT_USER, 
		"HKLM": wr.HKEY_LOCAL_MACHINE,
		"HKU": wr.HKEY_USERS,
		"HKCC": wr.HKEY_CURRENT_CONFIG
	}

	# https://msdn.microsoft.com/en-us/library/windows/desktop/bb773476(v=vs.85).aspx
	TYPE = {
		"BINARY": wr.REG_BINARY,
		"DWORD": wr.REG_DWORD,
		"QWORD": wr.REG_QWORD,
		"DWORD_LITTLE_ENDIAN": wr.REG_DWORD_LITTLE_ENDIAN,
		"QWORD_LITTLE_ENDIAN": wr.REG_QWORD_LITTLE_ENDIAN,
		"DWORD_BIG_ENDIAN": wr.REG_DWORD_BIG_ENDIAN,
		"EXPAND_SZ": wr.REG_EXPAND_SZ,
		"MULTI_SZ": wr.REG_MULTI_SZ,
		"NONE": wr.REG_NONE,
		"RESOURCE_LIST": wr.REG_RESOURCE_LIST,
		"SZ": wr.REG_SZ
	}


	# .:: METHOD ROOT ::.

	@classmethod
	def getRootKeys(cls):
		return Registry.ROOT.keys()

	
	@classmethod
	def getRootValue(cls):
		return Registry.ROOT.values()



	@classmethod
	def inRoot(cls, key):

		if isinstance(key, int):
			if key in Registry.getRootValue():
				return True
		
		return False

	

	@classmethod
	def inRootValue(cls, value):
		return True if value in Registry.getRootValue() else False



	# .:: METHOD TYPE ::.

	@classmethod
	def getTypeKeys(cls):
		return Registry.TYPE.keys()

	

	@classmethod 
	def getTypeValue(cls):
		return Registry.TYPE.values()



	# .:: METHOD REGISTRY ::.

	@classmethod
	def pathExist(cls, key, path):

		if Registry.inRoot(key):
			if isinstance(path, str):
				try:
					with wr.OpenKey(key, path) as _:
						return True
				except FileNotFoundError:
					return False



	@classmethod
	def valueExist(cls, key, path, value):

		if Registry.pathExist(key, path):
			if isinstance(value, str):
				try:
					with wr.OpenKey(key, path) as hkey:
						wr.QueryValueEx(hkey, value)[0]
						return True
				except FileNotFoundError:
					return False



	@classmethod
	def getEnumValue(cls, path, key):

		"""Retrieve the values"""

		if Registry.pathExist(key, path):
			if Registry.inRootValue(key):

				dataEnumValue = []

				try:
					with wr.OpenKey(key, path) as hkey:

						index = 0

						while 1:
							dataEnumValue.append(wr.EnumValue(hkey, index))
							index += 1
				except OSError:
					pass
				finally:
					if dataEnumValue:
						return dataEnumValue



	@classmethod
	def getEnumKey(cls, path, key, recurse=False, dataEnumKey=[]):

		"""List the keys and subkeys recursively"""

		if not recurse:
			dataEnumKey = []

		try:
			with wr.OpenKey(key, path) as hkey:

				index = 0

				while 1:

					absolutePath = path + "\\" + wr.EnumKey(hkey, index)

					if absolutePath[0] == "\\":
						absolutePath = absolutePath[1:]

					dataEnumKey.append((key, absolutePath))

					if recurse:
						Registry.getEnumKey(absolutePath, key, True, dataEnumKey)

					index += 1	
		except:
			pass
		finally:
			if dataEnumKey:
				return dataEnumKey



	@classmethod
	def getValue(cls, path, key, names):

		"""Search for one or more values of a key."""

		lock = 0


		if isinstance(path, str):
			lock += 1


		if isinstance(key, int):
			if key in Registry.ROOT:
				lock += 1


		if isinstance(names, (tuple, list, str)):
			if names:
				lock += 1


		if lock == 3:

			dataValue = {}

			try:
				with wr.OpenKey(key, path) as hkey:

					if isinstance(names, str):
						dataValue[names] = wr.QueryValueEx(hkey, names)[0]

					elif isinstance(names, (tuple, list)):
						for name in names:
							dataValue[name] = wr.QueryValueEx(hkey, name)[0]
					else:
						pass
			except:
				pass
			finally:
				if dataValue:
					return dataValue



	@classmethod
	def searchEnumKey(cls, path, key, recurse=False, search=[], dataSearch=[], show=False):

		try:
			with wr.OpenKey(key, path) as hkey:

				index = 0

				while True:

					absolutePath = path + "\\" + wr.EnumKey(hkey, index)

					if absolutePath[0] == "\\":
						absolutePath = absolutePath[1:]

					if search:
						if absolutePath.split("\\")[-1] in search:
							dataSearch.append((key, absolutePath))

					if show:
						print(absolutePath)

					if recurse:
						if search:
							Registry.searchEnumKey(absolutePath, key, True, search, dataSearch, False)
						elif show:
							Registry.searchEnumKey(absolutePath, key, True, None, None, True)
						else:
							Registry.searchEnumKey(absolutePath, key, True)

					index += 1	
		except:
			pass
		finally:
			if dataSearch:
				return dataSearch